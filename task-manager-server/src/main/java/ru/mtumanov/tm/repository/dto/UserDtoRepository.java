package ru.mtumanov.tm.repository.dto;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.mtumanov.tm.api.repository.dto.IDtoUserRepository;
import ru.mtumanov.tm.api.service.IConnectionService;
import ru.mtumanov.tm.dto.model.UserDTO;

import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;
import javax.persistence.NoResultException;
import java.util.Comparator;
import java.util.List;

public class UserDtoRepository extends AbstractDtoRepository<UserDTO> implements IDtoUserRepository {

    public UserDtoRepository(@NotNull final IConnectionService connectionService) {
        super(connectionService);
    }

    @Override
    public void clear() {
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        @NotNull final EntityTransaction transaction = entityManager.getTransaction();
        transaction.begin();
        try {
            entityManager.createQuery("DELETE FROM UserDTO")
                    .executeUpdate();
            transaction.commit();
        } catch (@NotNull final Exception e) {
            transaction.rollback();
            throw e;
        }
    }

    @Override
    @NotNull
    public List<UserDTO> findAll() {
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        return entityManager.createQuery("FROM UserDTO p", UserDTO.class)
                .getResultList();
    }


    @Override
    @NotNull
    public List<UserDTO> findAll(@NotNull final Comparator<UserDTO> comparator) {
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        return entityManager.createQuery("FROM UserDTO p ORDER BY p." + getComporator(comparator), UserDTO.class)
                .getResultList();
    }

    @Override
    @Nullable
    public UserDTO findOneById(@NotNull final String id) {
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        return entityManager.find(UserDTO.class, id);
    }

    @Override
    public long getSize() {
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        return entityManager.createQuery("SELECT COUNT(1) FROM UserDTO p", Long.class)
                .getSingleResult();
    }

    @Override
    public void removeById(@NotNull final String id) {
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        @NotNull final EntityTransaction transaction = entityManager.getTransaction();
        transaction.begin();
        try {
            entityManager.createQuery("DELETE FROM UserDTO WHERE id = :id")
                    .setParameter("id", id)
                    .executeUpdate();
            transaction.commit();
        } catch (@NotNull final Exception e) {
            transaction.rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @Override
    public boolean existById(@NotNull final String id) {
        try {
            findOneById(id);
        } catch (@NotNull final NoResultException e) {
            return false;
        }
        return true;
    }

    @Override
    public @NotNull UserDTO findByLogin(@NotNull final String login) {
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        return entityManager.createQuery("FROM UserDTO p WHERE login = :login", UserDTO.class)
                .setParameter("login", login)
                .getSingleResult();
    }

    @Override
    public @NotNull UserDTO findByEmail(@NotNull final String email) {
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        return entityManager.createQuery("FROM UserDTO p WHERE email = :email", UserDTO.class)
                .setParameter("email", email)
                .getSingleResult();
    }

    @Override
    public boolean isLoginExist(@NotNull final String login) {
        try {
            findByLogin(login);
        } catch (@NotNull final NoResultException e) {
            return false;
        }
        return true;
    }

    @Override
    public boolean isEmailExist(@NotNull final String email) {
        try {
            findByEmail(email);
        } catch (@NotNull final NoResultException e) {
            return false;
        }
        return true;
    }

}
