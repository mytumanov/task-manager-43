package ru.mtumanov.tm.repository.model;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.mtumanov.tm.api.repository.model.ITaskRepository;
import ru.mtumanov.tm.api.service.IConnectionService;
import ru.mtumanov.tm.model.Task;

import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;
import javax.persistence.NoResultException;
import java.util.Comparator;
import java.util.List;

public class TaskRepository extends AbstractUserOwnedRepository<Task> implements ITaskRepository {

    @NotNull
    private static final String USER_ID = "userId";

    @NotNull
    private static final String ID = "id";

    public TaskRepository(@NotNull IConnectionService connectionService) {
        super(connectionService);
    }

    @Override
    public void clear(@NotNull final String userId) {
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        @NotNull final EntityTransaction transaction = entityManager.getTransaction();
        transaction.begin();
        try {
            entityManager.createQuery("DELETE FROM Task WHERE userId = :userId")
                    .setParameter(USER_ID, userId)
                    .executeUpdate();
            transaction.commit();
        } catch (@NotNull final Exception e) {
            transaction.rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @Override
    @NotNull
    public List<Task> findAll(@NotNull final String userId) {
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        return entityManager.createQuery("FROM Task p WHERE p.userId = :userId", Task.class)
                .setParameter(USER_ID, userId)
                .getResultList();
    }

    @Override
    @NotNull
    public List<Task> findAll(@NotNull final String userId, @NotNull final Comparator<Task> comparator) {
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        return entityManager.createQuery("FROM Task p WHERE p.userId = :userId ORDER BY p." + getComporator(comparator), Task.class)
                .setParameter(USER_ID, userId)
                .getResultList();
    }

    @Override
    @Nullable
    public Task findOneById(@NotNull final String userId, @NotNull final String id) {
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        return entityManager.createQuery("FROM Task p WHERE p.userId = :userId AND p.id = :id", Task.class)
                .setParameter(USER_ID, userId)
                .setParameter(ID, id)
                .setMaxResults(1).getSingleResult();
    }

    @Override
    public long getSize(@NotNull final String userId) {
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        return entityManager.createQuery("SELECT COUNT(1) FROM Task p WHERE p.userId = :userId", Long.class)
                .setParameter(USER_ID, userId)
                .getSingleResult();
    }

    @Override
    public void removeById(@NotNull final String userId, @NotNull final String id) {
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        @NotNull final EntityTransaction transaction = entityManager.getTransaction();
        transaction.begin();
        try {
            entityManager.createQuery("DELETE FROM Task p WHERE p.userId = :userId AND p.id = :id")
                    .setParameter(USER_ID, userId)
                    .setParameter(ID, id)
                    .executeUpdate();
            transaction.commit();
        } catch (@NotNull final Exception e) {
            transaction.rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @Override
    public boolean existById(@NotNull final String userId, @NotNull final String id) {
        try {
            findOneById(userId, id);
        } catch (@NotNull final NoResultException e) {
            return false;
        }
        return true;
    }

    @Override
    public void clear() {
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        @NotNull final EntityTransaction transaction = entityManager.getTransaction();
        transaction.begin();
        try {
            entityManager.createQuery("DELETE FROM Task")
                    .executeUpdate();
            transaction.commit();
        } catch (@NotNull final Exception e) {
            transaction.rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @Override
    @NotNull
    public List<Task> findAll() {
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        return entityManager.createQuery("FROM Task p", Task.class)
                .getResultList();
    }


    @Override
    @NotNull
    public List<Task> findAll(@NotNull final Comparator<Task> comparator) {
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        return entityManager.createQuery("FROM Task p ORDER BY p." + getComporator(comparator), Task.class)
                .getResultList();
    }

    @Override
    @Nullable
    public Task findOneById(@NotNull final String id) {
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        return entityManager.find(Task.class, id);
    }

    @Override
    public long getSize() {
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        return entityManager.createQuery("SELECT COUNT(1) FROM Task p", Long.class)
                .getSingleResult();
    }

    @Override
    public void removeById(@NotNull final String id) {
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        @NotNull final EntityTransaction transaction = entityManager.getTransaction();
        transaction.begin();
        try {
            entityManager.createQuery("DELETE FROM Task WHERE id = :id")
                    .setParameter(ID, id)
                    .executeUpdate();
            transaction.commit();
        } catch (@NotNull final Exception e) {
            transaction.rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @Override
    public boolean existById(@NotNull final String id) {
        try {
            findOneById(id);
        } catch (@NotNull final NoResultException e) {
            return false;
        }
        return true;
    }

    @Override
    @NotNull
    public List<Task> findAllByProjectId(@NotNull final String userId, @NotNull final String projectId) {
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        return entityManager.createQuery("FROM Task t WHERE userId = :userId AND projectId = :projectId", Task.class)
                .setParameter(USER_ID, userId)
                .setParameter("projectId", projectId)
                .getResultList();
    }

    @Override
    public void removeTaskByProjectId(@NotNull final String projectId) {
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        @NotNull final EntityTransaction transaction = entityManager.getTransaction();
        transaction.begin();
        try {
            entityManager.createQuery("DELETE FROM Task WHERE projectId = :projectId")
                    .setParameter("projectId", projectId)
                    .executeUpdate();
            transaction.commit();
        } catch (@NotNull final Exception e) {
            transaction.rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

}
