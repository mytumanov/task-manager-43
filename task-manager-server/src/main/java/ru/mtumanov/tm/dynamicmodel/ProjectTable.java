package ru.mtumanov.tm.dynamicmodel;

import org.jetbrains.annotations.NotNull;
import org.mybatis.dynamic.sql.SqlColumn;
import org.mybatis.dynamic.sql.SqlTable;

import java.sql.JDBCType;
import java.util.Date;

public final class ProjectTable extends SqlTable {

    public ProjectTable() {
        super("tm_project");
    }

    @NotNull
    public final SqlColumn<String> id = column("id", JDBCType.VARCHAR);

    @NotNull
    public final SqlColumn<Date> created = column("created", JDBCType.TIMESTAMP);

    @NotNull
    public final SqlColumn<String> name = column("name", JDBCType.VARCHAR);

    @NotNull
    public final SqlColumn<String> description = column("description", JDBCType.VARCHAR);

    @NotNull
    public final SqlColumn<String> status = column("status", JDBCType.VARCHAR);

    @NotNull
    public final SqlColumn<String> userId = column("user_id", JDBCType.VARCHAR);

}
