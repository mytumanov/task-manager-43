package ru.mtumanov.tm.service.model;

import org.jetbrains.annotations.NotNull;
import ru.mtumanov.tm.api.repository.model.IRepository;
import ru.mtumanov.tm.api.service.IConnectionService;
import ru.mtumanov.tm.api.service.model.IService;
import ru.mtumanov.tm.exception.AbstractException;
import ru.mtumanov.tm.model.AbstractModel;

import javax.persistence.EntityManager;
import java.util.Collection;
import java.util.List;

public abstract class AbstractService<M extends AbstractModel, R extends IRepository<M>> implements IService<M> {

    @NotNull
    final IConnectionService connectionService;

    @NotNull
    protected final R repository;

    protected AbstractService(@NotNull final IConnectionService connectionService) {
        this.connectionService = connectionService;
        repository = getRepository();
    }

    @NotNull
    private EntityManager getEntityManager() {
        return connectionService.getEntityManager();
    }

    @NotNull
    protected abstract R getRepository();

    @Override
    public @NotNull List<M> findAll() {
        return repository.findAll();
    }

    @Override
    @NotNull
    public void set(@NotNull Collection<M> models) throws AbstractException {
        for (M model : models) {
            repository.add(model);
        }
    }

}
