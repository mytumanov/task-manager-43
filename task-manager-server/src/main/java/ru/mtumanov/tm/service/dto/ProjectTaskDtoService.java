package ru.mtumanov.tm.service.dto;

import org.jetbrains.annotations.NotNull;
import ru.mtumanov.tm.api.repository.dto.IDtoProjectRepository;
import ru.mtumanov.tm.api.repository.dto.IDtoTaskRepository;
import ru.mtumanov.tm.api.service.IConnectionService;
import ru.mtumanov.tm.api.service.dto.IDtoProjectTaskService;
import ru.mtumanov.tm.dto.model.TaskDTO;
import ru.mtumanov.tm.exception.AbstractException;
import ru.mtumanov.tm.exception.entity.ProjectNotFoundException;
import ru.mtumanov.tm.exception.field.IdEmptyException;
import ru.mtumanov.tm.repository.dto.ProjectDtoRepository;
import ru.mtumanov.tm.repository.dto.TaskDtoRepository;

import java.util.List;

public class ProjectTaskDtoService implements IDtoProjectTaskService {

    @NotNull
    private final IDtoTaskRepository taskRepository;

    @NotNull
    private final IDtoProjectRepository projectRepository;

    public ProjectTaskDtoService(@NotNull final IConnectionService connectionService) {
        this.taskRepository = new TaskDtoRepository(connectionService);
        this.projectRepository = new ProjectDtoRepository(connectionService);
    }

    @Override
    public void bindTaskToProject(@NotNull final String userId, @NotNull final String projectId, @NotNull final String taskId) throws AbstractException {
        if (projectId.isEmpty())
            throw new IdEmptyException();
        if (taskId.isEmpty())
            throw new IdEmptyException();
        if (!projectRepository.existById(userId, projectId))
            throw new ProjectNotFoundException();

        @NotNull final TaskDTO task = taskRepository.findOneById(userId, taskId);
        task.setProjectId(projectId);
        taskRepository.update(task);
    }

    @Override
    public void removeProjectById(@NotNull final String userId, @NotNull final String projectId) throws AbstractException {
        if (projectId.isEmpty())
            throw new IdEmptyException();
        if (!projectRepository.existById(userId, projectId))
            throw new ProjectNotFoundException();

        @NotNull final List<TaskDTO> tasks = taskRepository.findAllByProjectId(userId, projectId);
        for (TaskDTO task : tasks)
            taskRepository.removeById(task.getId());
        projectRepository.removeById(userId, projectId);
    }

    @Override
    public void unbindTaskFromProject(@NotNull final String userId, @NotNull final String projectId, @NotNull final String taskId) throws AbstractException {
        if (projectId.isEmpty())
            throw new IdEmptyException();
        if (taskId.isEmpty())
            throw new IdEmptyException();
        if (!projectRepository.existById(userId, projectId))
            throw new ProjectNotFoundException();

        @NotNull final TaskDTO task = taskRepository.findOneById(userId, taskId);
        task.setProjectId(null);
        taskRepository.update(task);
    }

}
