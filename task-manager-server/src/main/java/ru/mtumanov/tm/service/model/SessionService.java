package ru.mtumanov.tm.service.model;

import org.jetbrains.annotations.NotNull;
import ru.mtumanov.tm.api.repository.model.ISessionRepository;
import ru.mtumanov.tm.api.service.IConnectionService;
import ru.mtumanov.tm.api.service.model.ISessionService;
import ru.mtumanov.tm.model.Session;
import ru.mtumanov.tm.repository.model.SessionRepository;

public class SessionService extends AbstractUserOwnedService<Session, ISessionRepository> implements ISessionService {

    protected SessionService(@NotNull final IConnectionService connectionService) {
        super(connectionService);
    }

    @Override
    @NotNull
    protected ISessionRepository getRepository() {
        return new SessionRepository(connectionService);
    }

}
