package ru.mtumanov.tm.exception.server;

import org.jetbrains.annotations.NotNull;

public class TMConnectExceptio extends AbstractServerException {

    public TMConnectExceptio() {
        super("ERROR! Can't connect to server!");
    }

    public TMConnectExceptio(@NotNull final String message) {
        super(message);
    }

}
