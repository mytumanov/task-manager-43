package ru.mtumanov.tm.dto.response.project;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.mtumanov.tm.dto.model.ProjectDTO;

@NoArgsConstructor
public final class ProjectCreateRs extends AbstractProjectRs {

    public ProjectCreateRs(@Nullable final ProjectDTO project) {
        super(project);
    }

    public ProjectCreateRs(@NotNull final Throwable err) {
        super(err);
    }

}